import { Component, OnInit } from '@angular/core';
import { GovernmentBillChartData, Series, MULTI } from './data';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
    governmentBillChartData: GovernmentBillChartData[];
  governmentBillChartData_horizontal_stacked: GovernmentBillChartData[];

  ngOnInit() {
    this.governmentBillChartData = MULTI;
  }
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Bill Reading Votes in Parliament';
  showYAxisLabel = true;
  yAxisLabel = 'Party Votes';
  tooltipDisabled = false;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  colorScheme_horizontal_stacked = {
    domain: [
      '#FDE401',
      '#098137',
      '#d82a20',
      '#EF4A42',
      '#00529F',
      '#000000',
      '#501557']
  };

  constructor() {  }
}
