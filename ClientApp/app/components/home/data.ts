export interface GovernmentBillChartData {
  name: string;
  barColor: string;
  series: Series[];
}

export interface Series {
  name: string;
  value: number;
}


export const SINGLE = [
  {
    'name': 'Germany',
    'value': 8940000
  },
  {
    'name': 'USA',
    'value': 5000000
  },
  {
    'name': 'France',
    'value': 7200000
  }
];

export const MULTI: GovernmentBillChartData[] = [
  {
    'name': 'National',
    'barColor': '#0000ff',
    'series': [
      {
        'name': 'Reading 1',
        'value': -42
      },
      {
        'name': 'Reading 2',
        'value': 42
      },
      {
        'name': 'Reading 3',
        'value': 42
      }
    ]
  },
  {
    'name': 'Labour',
    'barColor': '#ff0000',
    'series': [
      {
        'name': 'Reading 1',
        'value': 30
      },
      {
        'name': 'Reading 2',
        'value': 30
      },
      {
        'name': 'Reading 3',
        'value': 30
      }
    ]
  },
  {
    'name': 'NZ First',
    'barColor': '#000000',
    'series': [
      {
        'name': 'Reading 1',
        'value': 14
      },
      {
        'name': 'Reading 2',
        'value': 14
      },
      {
        'name': 'Reading 3',
        'value': 14
      }
    ]
  },
  {
    'name': 'Green Party',
    'barColor': '#ff00ff',
    'series': [
      {
        'name': 'Reading 1',
        'value': 10
      },
      {
        'name': 'Reading 2',
        'value': 10
      },
      {
        'name': 'Reading 3',
        'value': 10
      }
    ]
  }
];
